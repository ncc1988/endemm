/*
    DVDResource.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "DVDResource.h"


using namespace Endemm;


DVDResource::DVDResource(const std::string& location)
    : Resource(location)
{
    //DVD media are read-only:
    this->read_access  = true;
    this->write_access = false;

    //Convert the location to title and optional chapters:
    auto location_parts = StringHelper::explode(":", location, 1);
    if (location_parts.empty()) {
        //Throw exception.
        throw Exception("Invalid location specified.");
    }
    if (location_parts.size() >= 1) {
        this->dvd_unit_number = std::stoi(location_parts[0]);
        this->resource_unit   = DVDUnit::TITLE;
    }
    if (location_parts.size() >= 2) {
        //A start chapter is specified, maybe even an end chapter.
        auto chapters = StringHelper::explode("-", location_parts[1], 1);
        if (chapters.size() > 0) {
            this->position_uom = DVDUnit::CHAPTER;
            this->dvd_start_position = std::stoi(chapters[0]);
            if (chapters.size() > 1) {
                //An end chapter is specified.
                this->dvd_end_position = std::stoi(chapters[1]);
            }
        }
    }
}


DVDResource::~DVDResource()
{
    if (this->dvd_file_handle) {
        DVDCloseFile(this->dvd_file_handle);
    }
    if (this->handle) {
        DVDClose(this->handle);
    }
}


void DVDResource::setDVDDevice(const std::string& dvd_device)
{
    this->dvd_device = dvd_device;
}


std::string DVDResource::getDVDDevice()
{
    return this->dvd_device;
}


uint64_t DVDResource::getStartSector()
{
    return this->dvd_start_sector;
}


uint64_t DVDResource::getEndSector()
{
    return this->dvd_end_sector;
}


uint64_t DVDResource::getCurrentSector()
{
    return this->dvd_current_sector;
}


bool DVDResource::initialise()
{
    if (this->dvd_device.empty()) {
        //Without a DVD device name, we cannot create a DVD handle.
        return false;
    }
    this->handle = DVDOpen(this->dvd_device.c_str());
    if (this->handle == 0) {
        //Nothing to read from.
        return false;
    }

    auto ifo_handle = ifoOpen(this->handle, 0);
    if (ifo_handle == nullptr) {
        //Read failure.
        return false;
    }

    if (this->resource_unit == DVDUnit::TITLE) {
        //Read title information.
        if (ifo_handle->tt_srpt == nullptr) {
            //No title information available.
            ifoClose(ifo_handle);
            return false;
        }
        if (ifo_handle->tt_srpt->nr_of_srpts < 1 || ifo_handle->tt_srpt->title == nullptr) {
            //No titles or no title information array.
            ifoClose(ifo_handle);
            return false;
        }
        //Check if the specified title number is valid:
        if (this->dvd_unit_number > ifo_handle->tt_srpt->nr_of_srpts) {
            //The title number is out of range.
            ifoClose(ifo_handle);
            return false;
        }

        //Get the title information:
        auto title_info = ifo_handle->tt_srpt->title[this->dvd_unit_number - 1];

        //Extract the relevant data:
        uint8_t title_set_number       = (uint32_t)title_info.title_set_nr;
        uint8_t title_set_title_number = (uint8_t)title_info.vts_ttn;
        uint16_t number_of_chapters    = (uint16_t)title_info.nr_of_ptts;

        //Read information from the title set:
        auto title_set_ifo_handle = ifoOpen(handle, title_set_number);
        if (title_set_ifo_handle == nullptr) {
            //Read failure.
            ifoClose(ifo_handle);
            return false;
        }

        //Get the start and end sector for the title by looking at the
        //program chain table and the entry for the title in it:

        ifoRead_PGCIT(title_set_ifo_handle);
        if (title_set_ifo_handle->vts_pgcit == nullptr) {
            //Read error.
            ifoClose(title_set_ifo_handle);
            ifoClose(ifo_handle);
            return false;
        }
        if (this->dvd_unit_number > title_set_ifo_handle->vts_pgcit->nr_of_pgci_srp) {
            //Title out of range.
            ifoClose(title_set_ifo_handle);
            ifoClose(ifo_handle);
            return false;
        }

        auto title_program_data = title_set_ifo_handle->vts_pgcit->pgci_srp[this->dvd_unit_number - 1];
        if (title_program_data.pgc == nullptr) {
            //Reading of the start and end sector of the title failed.
            ifoClose(title_set_ifo_handle);
            ifoClose(ifo_handle);
            return false;
        }

        if (title_program_data.pgc == nullptr) {
            //No cell information for the DVD title.
            ifoClose(title_set_ifo_handle);
            ifoClose(ifo_handle);
            return false;
        }
        auto title_data = title_program_data.pgc;
        if (title_data->nr_of_cells < 1) {
            //The DVD title has no cells.
            ifoClose(title_set_ifo_handle);
            ifoClose(ifo_handle);
            return false;
        }

        //The start sector is the first sector of the first cell.
        //If there is only one cell, the end sector is the last sector
        //of that cell:
        this->dvd_start_sector = (uint64_t)title_data->cell_playback[0].first_sector;
        this->dvd_end_sector   = title_data->cell_playback[0].last_sector;
        if (title_data->nr_of_cells > 1) {
            //More than one cell. This means that the end sector has to be
            //calculated differently to avoid reading DVD menu data or other
            //non-video data. This is done by assuming continuity for DVD video
            //titles so that there is no jumping on the disk: Each cell follows
            //directly after the last one (no gaps) if it contains video data.
            for (uint16_t i = 1; i < title_data->nr_of_cells; i++) {
                if (title_data->cell_playback[i].first_sector == this->dvd_end_sector + 1) {
                    //The cell starts directly after the last cell. Use its last
                    //sector and continue.
                    this->dvd_end_sector = (uint64_t)title_data->cell_playback[i].last_sector;
                } else {
                    //The cell does not start directly after the last cell.
                    //This must be a DVD menu or other non-video data.
                    //At this point, the last sector has been found and the loop
                    //can be exited.
                    break;
                }
            }
        }

        ifoClose(title_set_ifo_handle);

        this->dvd_file_handle = DVDOpenFile(this->handle, title_set_number, DVD_READ_TITLE_VOBS);
        if (this->dvd_file_handle == 0) {
            //Cannot open file.
            ifoClose(ifo_handle);
            return false;
        }
    } else if (this->resource_unit == DVDUnit::SECTOR) {
        //Set start and end sector directly from :
        this->dvd_start_sector = this->dvd_start_position;
        this->dvd_end_sector   = this->dvd_end_position;
    } else {
        //Other types of DVD units (chapters, sectors) are not supported at the moment.
        ifoClose(ifo_handle);
        return false;
    }

    ifoClose(ifo_handle);

    return true;
}


std::shared_ptr<Container> DVDResource::probe()
{
    //This is simple: DVD video always has a MPEGPSContainer:
    //return std::make_shared<MPEGPSContainer>(this->shared_from_this());
    return nullptr; //Not yet implemented.
}


bool DVDResource::writeObject(const uint8_t* data, size_t size)
{
    //DVD video discs are not writable (at least in this context).
    return false;
}


bool DVDResource::writeObject(const uint8_t* data, size_t size, size_t position)
{
    //DVD video discs are not writable (at least in this context).
    return false;
}


bool DVDResource::readObject(const uint8_t* data, size_t size)
{
    //TODO
    return false;
}


bool DVDResource::readObject(const uint8_t* data, size_t size, size_t position)
{
    //TODO
    return false;
}


size_t DVDResource::write(const std::vector<uint8_t>& data)
{
    //DVD video discs are not writable (at least in this context).
    return 0;
}


size_t DVDResource::write(const std::vector<uint8_t>& data, size_t position)
{
    //DVD video discs are not writable (at least in this context).
    return 0;
}


std::vector<uint8_t> DVDResource::read(size_t size)
{
    if (this->dvd_file_handle == nullptr) {
        if (!this->initialise()) {
            //The DVD file handle cannot be used for reading.
            return {};
        }
    }

    if (size <= this->last_read_leftover.size()) {
        //Extract the requested data from last_read_leftover
        //and return them directly:
        auto begin_it = this->last_read_leftover.begin();
        auto end_it = this->last_read_leftover.begin();
        std::advance(end_it, size);
        std::vector<uint8_t> chunk(begin_it, end_it);
        if (std::distance(end_it, this->last_read_leftover.end()) > 1) {
            //There is at least one more byte to be left over
            //for the next call of the read method.
            end_it = std::next(end_it);
            this->last_read_leftover = std::vector<uint8_t>(end_it, this->last_read_leftover.end());
        } else {
            //No bytes left over.
            this->last_read_leftover.clear();
        }
        return chunk;
    }

    uint64_t blocks_to_read = size / 2048;
    if (size % 2048 > 0) {
        //Reading does not happen in 2048 blocks:
        //One more block has to be read entirely
        //but not all of its content is returned.
        blocks_to_read++;
    }
    if (this->dvd_current_sector + blocks_to_read > this->dvd_end_sector) {
        //Requested reading out of range: Keep it in range instead.
        blocks_to_read = this->dvd_end_sector - this->dvd_current_sector;
    }
    if (!this->last_read_leftover.empty()) {
        //There is less data to read in case the amount of leftover bytes
        //is bigger than one DVD block.
        blocks_to_read -= (this->last_read_leftover.size() / 2048);
    }
    if (blocks_to_read > 0) {
        if (this->dvd_current_sector == 0) {
            //The current sector is not set: Set it to the start sector.
            //Even if that is 0, dvd_current_sector will not stay at 0 after
            //the first reading did happen.
            this->dvd_current_sector = this->dvd_start_sector;
        }

        //NOTE: $buffer_size is not necessarily the same as $size, since
        //reading happens aligned to 2048 byte blocks.
        size_t buffer_size = blocks_to_read * 2048;

        if (this->read_buffer.size() < buffer_size) {
            //Make the buffer larger:
            this->read_buffer.resize(buffer_size, 0);
        }

        auto blocks_read = DVDReadBlocks(this->dvd_file_handle, this->dvd_current_sector, blocks_to_read, this->read_buffer.data());
        if (blocks_read == -1) {
            //Reading failed.
            return {};
        }
        //Update the current sector:
        this->dvd_current_sector += blocks_read;
    }

    if (this->read_buffer.size() == size && this->last_read_leftover.empty()) {
        //The buffer can be returned directly as a copy. But before returning,
        //the read buffer must be cleared to prevent returned data to be present
        //in the next iteration.
        auto buffer = std::move(this->read_buffer);
        this->read_buffer.clear();
        return buffer;
    } else if (this->read_buffer.size() > size) {
        //More data have been read than needed.
        //Return only the part of the data that is needed:
        auto size_it = this->read_buffer.begin();
        //Do not necessarily advance to the full size of the read buffer,
        //when there are leftover bytes:
        std::advance(size_it, size - this->last_read_leftover.size());
        std::vector<uint8_t> data(this->read_buffer.begin(), size_it);
        //Append the leftover bytes:
        size_t required_leftover_size = size - data.size();
        if (required_leftover_size <= this->last_read_leftover.size()) {
            //A less or equal amount of bytes is required from the
            //leftover byte array.
            auto required_leftover_it = this->last_read_leftover.begin();
            std::advance(required_leftover_it, required_leftover_size);
            //Append the data:
            data.insert(data.end(), this->last_read_leftover.begin(), required_leftover_it);
            //Remove the used leftover bytes:
            //TODO
        }

        //Put the leftover in the last_read_leftover attribute:
        size_it = std::next(size_it);
        if (size_it != this->read_buffer.end()) {
            //There are leftover bytes.
            this->last_read_leftover.resize(
                std::distance(size_it, this->read_buffer.end()),
                0
                );
            this->last_read_leftover.insert(this->last_read_leftover.begin(), size_it, this->read_buffer.end());
        }

        return data;
    }
    //Unhandled cases:
    return {};
}


std::vector<uint8_t> DVDResource::read(size_t size, size_t position)
{
    //TODO: implement jumping to position, then call read.
    return {};
}


bool DVDResource::seekAbsolutePosition(size_t position)
{
    //TODO
    return false;
}


bool DVDResource::moveForward(size_t offset)
{
    //TODO
    return false;
}


bool DVDResource::moveBackward(size_t offset)
{
    //TODO
    return false;
}
