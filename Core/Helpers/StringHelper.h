/*
    StringHelper.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__STRINGHELPER_H
#define ENDEMM__STRINGHELPER_H


#include <string>
#include <vector>


namespace Endemm
{
    class StringHelper
    {
        public:


        /**
         * This methods splits a string by a separator and returns a list of
         * strings with the last element holding the remainder of the input.
         *
         * This method gets its name from the PHP explode function that does
         * the same thing.
         *
         * @param const std::string separator The string on which to split
         *     the input.
         *
         * @param const std::string input The input string.
         *
         * @param const uint32_t limit The amount of splits to make.
         *     If this is set to zero, the amount of splits is not limited.
         */
        static std::vector<std::string> explode(
            const std::string separator,
            const std::string input,
            const uint32_t limit = 0
            );
    };
}


#endif
