/*
    SimpleResourceProber.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "SimpleResourceProber.h"


using namespace Endemm;


std::shared_ptr<Container> SimpleResourceProber::probeResource(std::shared_ptr<Resource> resource)
{
    if (resource == nullptr) {
        //Nothing we can probe.
        return nullptr;
    }
    //This prober is not doing much at the moment:
    return std::make_shared<RIFFContainer>(resource);
}
