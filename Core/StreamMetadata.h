/*
    StreamMetadata.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__STREAMMETADATA_H
#define ENDEMM__STREAMMETADATA_H


#include <string>


namespace Endemm
{
    /**
     * This class represents metadata for a stream.
     */
    class StreamMetadata
    {
        public:

        /**
         * The language of the stream.
         *
         * NOTE: This is not yet standardised in Endemm, so that it may contain
         * a 2-letter code, 3-letter code or the full name of the language.
         * It is likely that a 3-letter code will be used in the future, so
         * users of this class are encouraged to use 3-letter codes.
         */
        std::string language;
    };
}


#endif
