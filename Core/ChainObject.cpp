/*
    ChainObject.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ChainObject.h"


using namespace Endemm;


void ChainObject::receiveData(std::vector<uint8_t> data, bool last_chunk)
{
    auto result = this->processData(data);
    if (result.empty()) {
        return;
    }
    if (this->next_chain_objects.empty()) {
        //Call the endpoint method:
        this->sendDataToEndpoint(result, last_chunk);
    } else {
        //Send the data to the next objects in the chain:
        for (auto next: this->next_chain_objects) {
            next->receiveData(result, last_chunk);
        }
    }
}


bool ChainObject::isInChain(
    std::shared_ptr<ChainObject> needle,
    std::shared_ptr<ChainObject> haystack
    )
{
    if (needle == haystack) {
        return true;
    }

    if ((needle == nullptr) || (haystack == nullptr)) {
        return false;
    }

    for (auto next_haystack: haystack->getNextChainObjects()) {
        if (ChainObject::isInChain(needle, next_haystack)) {
            return true;
        }
    }
    //If code execution reaches this point, the needle could not be found.
    return false;
}


void ChainObject::addNextChainObject(std::shared_ptr<ChainObject> next)
{
    if (ChainObject::isInChain(this->shared_from_this(), next)) {
        //We cannot make a circular chain!
        return;
    }
    //Now we must check if next is already in the list of
    //next chain objects:
    for (auto object: this->next_chain_objects) {
        if (object == next) {
            //Nothing to do.
            return;
        }
    }

    //If code execution reaches this point, we can add the next chain
    //object to this object.
    this->next_chain_objects.push_back(next);
}


std::vector<std::shared_ptr<ChainObject>> ChainObject::getNextChainObjects()
{
    return this->next_chain_objects;
}
