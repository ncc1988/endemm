/*
    ContainerMetadata.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__CONTAINERMETADATA_H
#define ENDEMM__CONTAINERMETADATA_H


#include <cinttypes>
#include <string>


namespace Endemm
{
    /**
     * This class represents metadata for an entire container.
     */
    class ContainerMetadata
    {
        public:

        //General metadata:

        /**
         * The title of the media.
         */
        std::string title;

        /**
         * The description for the media.
         */
        std::string description;

        /**
         * The year for the copyright.
         */
        uint16_t copyright_year;

        /**
         * The copyright date as string.
         * The date format in this string can vary.
         */
        std::string copyright_date;

        /**
         * The URL of the license.
         */
        std::string license_url;

        /**
         * The text of the license.
         */
        std::string license_text;

        //Audio metadata:

        /**
         * The artist that created the media.
         */
        std::string artist;

        /**
         * The album of the media.
         */
        std::string album;

        //Video metadata:

        /**
         * The season (of a series).
         */
        std::string season;

        /**
         * The episode (of a series).
         */
        std::string episode;

        /**
         * The director of the video.
         */
        std::string director;

        /**
         * The producer of the video.
         */
        std::string producer;
    };
}


#endif
