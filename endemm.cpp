/*
    endemm - encoder and decoder for multimedia
    Copyright (C) 2016 - 2023 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <algorithm>
#include <memory>
#include <sstream>
#include <string>
#include <vector>


#include <fmt/core.h>


#include "Containers/RIFFContainer.h" //only in early stage development!
#include "Codecs/PCMCodec.h" //only in early stage development!

#include "Core/Exceptions.h"
#include "Core/FileResource.h"
#include "Core/ConversionManager.h"


using namespace Endemm;


void print_copyright_notice()
{
    std::cerr << "endemm  Copyright (C) 2016-2024  Moritz Strohm <ncc1988@posteo.de>\n";
}


void print_help_and_exit()
{
    print_copyright_notice();
    std::cerr <<
        "Usage: endem PARAMETERS\n\n"
        "Parameters:\n"
        "\t-q\tQuiet mode: no messages are being output.\n"
        "\t-json\tMessages are output as machine-readable JSON objects.\n"
        "\t-i\tSpecify an unnamed input.\n"
        "\t-i:NAME\tSpecify a named input where NAME is an arbitrary name for the input.\n"
        "\t-o\tSpecify an unnamed output.\n"
        "\t-o:NAME\tSpecify a named output where NAME is an arbitrary name for the output.\n"
        "\t-p\tSpecify a processing option. (NOT YET SUPPORTED)\n"
        "\t-acodec\tSet the audio codec for all audio channels in all outputs.\n"
        "\t-vcodec\tSet the video codec for all video channels in all outputs.\n"
        "\t-ab\tSet the bitrate for all audio channels in all outputs.\n"
        "\t-vb\tSet the bitrate for all video channels in all outputs.\n"
        "\t-aq\tSet the quality level (0-100) for all audio channels in all outputs.\n"
        "\t-vq\tSet the quality level (0-100) for all video channels in all outputs.\n"
              << std::endl;
    exit(EXIT_SUCCESS);
}


void print_codec(std::shared_ptr<Codec> codec)
{
    if (codec == nullptr) {
        return;
    }

    std::cout << fmt::format(
        "Codec: {0}\n",
        codec->getName()
        );
    /*
    if (stream.type == STREAM_AUDIO) {
        std::cout << fmt::format(
            "AUDIO data:\n"
            "----\n"
            "samples/second: {0}\n"
            "resolution: {1} bit\n"
            "channels: {2}\n",
            stream.audio_samples_per_second,
            stream.audio_resolution,
            stream.audio_channels
            );
    } else if (stream.type == STREAM_VIDEO) {
        std::cout << fmt::format(
            "VIDEO data:\n"
            "----\n"
            "resolution: {0}x{1}\n"
            "pixel_format: {2}\n",
            stream.video_width,
            stream.video_height,
            stream.video_pixel_format
            );
    } else if (stream.type == STREAM_TEXT) {
        std::cout <<
            "TEXT data:\n"
            "----\n";
    } else {
        std::cout << "UNKNOWN data!\n";
    }
    */
}


void print_resource(Resource& resource)
{
    std::cout << fmt::format(
        "Resource #{0}:\n"
        "--------\n"
        "Location: {1}\n"
        "Metadata:\n"
        //place content of resource.metadata below here.
        "----\n",
        resource.getId(),
        resource.getLocation()
        );
}


std::vector<std::string> getArgumentParts(const std::string& argument, const char delimiter = ':')
{
    if (argument.length() <= 1) {
        //An empty argument (only the "-" character).
        return {};
    }
    //Remove the first character from the argument, since we're
    //only interested in the content of the argument:
    std::stringstream stream(argument.substr(1));
    std::vector<std::string> parts;
    std::string current_part;
    while (std::getline(stream, current_part, delimiter)) {
        parts.push_back(current_part);
    }
    return parts;
}


enum class NextArgType
{
    NONE,

    GLOBAL,

    INPUT,

    OUTPUT,

    PROCESSING
};


int main(int argc, char** argv)
{
    std::vector<std::string> args(argv + 1, argv + argc);

    std::shared_ptr<ConversionManager> conversion_manager = std::make_shared<ConversionManager>();

    //Handle command line arguments:

    bool quiet = false;
    bool json_output = false;

    NextArgType next_arg_type = NextArgType::NONE;

    std::vector<std::string> last_arg_parts = {};

    if (args.size() < 1) {
        //No arguments specified.
        print_help_and_exit();
    }

    for (auto arg: args) {
        if (next_arg_type == NextArgType::NONE) {
            if (arg.empty() || arg.length() < 2) {
                continue;
            }
            if (arg == "--help") {
                print_help_and_exit();
            } else if (arg == "-q") {
                quiet = true;
                conversion_manager->disableTextOutput();
            } else if (arg == "-json") {
                json_output = true;
                conversion_manager->enableJsonOutput();
            } else if (arg[1] == 'i') {
                //An input option is specified in the next argument.
                next_arg_type = NextArgType::INPUT;
                //Split it by colons.
                last_arg_parts = getArgumentParts(arg);
            } else if (arg[1] == 'o') {
                //An output option is specified in the next argument.
                next_arg_type = NextArgType::OUTPUT;
                //Split it by colons.
                last_arg_parts = getArgumentParts(arg);
            } else if (arg[1] == 'p') {
                //An processing option is specified in the next argument.
                next_arg_type = NextArgType::OUTPUT;
                //Split it by colons.
                last_arg_parts = getArgumentParts(arg);
            } else if ((arg == "-acodec") || (arg == "-vcodec") || (arg == "-ab")
                       || (arg == "-vb") || (arg == "-aq") || (arg == "-vq")) {
                next_arg_type = NextArgType::GLOBAL;
                last_arg_parts = {arg};
            }
        } else if (next_arg_type == NextArgType::GLOBAL) {
            if (last_arg_parts.size() > 0) {
                if (last_arg_parts[0] == "-acodec") {
                    conversion_manager->setCodec(arg, "*:A*");
                } else if (last_arg_parts[0] == "-vcodec") {
                    conversion_manager->setCodec(arg, "*:V*");
                } else if (last_arg_parts[0] == "-ab") {
                    conversion_manager->setBitrate(arg, "*:A*");
                } else if (last_arg_parts[0] == "-vb") {
                    conversion_manager->setBitrate(arg, "*:V*");
                } else if (last_arg_parts[0] == "-aq") {
                    conversion_manager->setQuality(arg, "*:A*");
                } else if (last_arg_parts[0] == "-vq") {
                    conversion_manager->setQuality(arg, "*:V*");
                }
            }
            next_arg_type = NextArgType::NONE;
            last_arg_parts = {};
        } else if (next_arg_type == NextArgType::INPUT) {
            if (!arg.empty()) {
                if (last_arg_parts.size() == 1) {
                    if (last_arg_parts[0] == "i") {
                        //An unnamed input is specified.
                        conversion_manager->addInput(arg);
                    }
                } else if (last_arg_parts.size() == 2) {
                    //An option for a named input.
                    if (last_arg_parts[0] == "i") {
                        //A named input is specified.
                        conversion_manager->addInput(arg, last_arg_parts[1]);
                    }
                }
            }
            next_arg_type = NextArgType::NONE;
            last_arg_parts = {};
        } else if (next_arg_type == NextArgType::OUTPUT) {
            if (!arg.empty()) {
                //Invalid parameter. Do nothing.
                if (last_arg_parts.size() == 1) {
                    if (last_arg_parts[0] == "o") {
                        //An unnamed output is specified.
                        conversion_manager->addOutput(arg);
                    }
                } else if (last_arg_parts.size() == 2) {
                    //An option for a named output.
                    if (last_arg_parts[0] == "o") {
                        //A named output is specified.
                        conversion_manager->addOutput(arg, last_arg_parts[1]);
                    }
                }
            }
            next_arg_type = NextArgType::NONE;
            last_arg_parts = {};
        }
    }


    if (!quiet) {
        if (json_output) {
            std::cerr << "{\"msg_type\": \"program_info\", \"content\": {\"name\" : \"endemm\", \"copyright\": \"2016-2024\", \"author\": \"Moritz Strohm <ncc1988@posteo.de>\"}}\n";
        } else {
            print_copyright_notice();
        }
    }

    try {
        conversion_manager->convert();
    } catch (Exception& e) {
        if (!quiet) {
            std::cerr << fmt::format("Exception caught: {0}", e.getMessage()) << std::endl;
        }
        return 1;
    }

    return 0;
}
