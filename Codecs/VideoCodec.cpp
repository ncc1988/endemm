/*
    VideoCodec.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "VideoCodec.h"


using namespace Endemm;


VideoCodec::VideoCodec()
{
    //Nothing else.
}


VideoCodec::VideoCodec(const VideoStreamConfig& config, bool encode_mode)
{
    this->encode_mode = encode_mode;
    if (this->encode_mode) {
        this->output_stream_config = config;
    } else {
        this->input_stream_config = config;
    }
}


std::string VideoCodec::getName()
{
    //This codec just copies video data, so it gets the name "vcopy":
    return "vcopy";
}


bool VideoCodec::step(size_t size)
{
    if (this->encode_mode) {
        //Encoding cannot be the first step. We need data first
        //from somewhere else!
        return false;
    }
    /*
    if (!this->input_stream_config) {
        //We need a configuration to read data accurately!
        return false;
    }
    */
    auto data = this->readData(size);
    if (data.empty()) {
        return false;
    }
    //Pass the data to the receiveData method. If the data vector
    //has a smaller size than the requested data size, we assume
    //that this is the last chunk of data.
    this->receiveData(data, (data.size() < size));
    return true;
}


std::vector<uint8_t> VideoCodec::readData(size_t size)
{
    if (this->container.expired()) {
        return {};
    }
    auto container_ptr = this->container.lock();
    if (container_ptr == nullptr) {
        return {};
    }
    return container_ptr->readVideoData(this->stream_id, size);
}


void VideoCodec::writeData(const std::vector<uint8_t>& data, bool last_chunk)
{
    if (this->container.expired()) {
        return;
    }
    auto container_ptr = this->container.lock();
    if (container_ptr == nullptr) {
        return;
    }
    container_ptr->writeAudioData(this->stream_id, data);
    if (last_chunk) {
        container_ptr->updateHeader();
    }
}


//VideoChainObject implementatiton:


std::vector<VideoColourSpace> VideoCodec::getInputColourSpaces()
{
    return {
        VideoColourSpace::RGB24,
        VideoColourSpace::RGBA32,
        VideoColourSpace::RGB565,
        VideoColourSpace::RGB48,
        VideoColourSpace::RGBA64
    };
}


std::vector<VideoColourSpace> VideoCodec::getOutputColourSpace()
{
    return {
        VideoColourSpace::RGB24,
        VideoColourSpace::RGBA32,
        VideoColourSpace::RGB565,
        VideoColourSpace::RGB48,
        VideoColourSpace::RGBA64
    };
}


//ChainObject implementation:


void VideoCodec::sendDataToEndpoint(std::vector<uint8_t> data, bool last_chunk)
{
    this->writeData(data, last_chunk);
}


std::vector<uint8_t> VideoCodec::processData(std::vector<uint8_t> input)
{
    //Just output the input data. Very simple, isn't it?
    return input;
}
